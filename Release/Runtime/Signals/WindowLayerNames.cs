namespace Playdarium.SimpleUI.Runtime.Signals
{
	public class WindowLayerNames
	{
		public const string Project = "Project";
		public const string Local = "Local";
	}
}