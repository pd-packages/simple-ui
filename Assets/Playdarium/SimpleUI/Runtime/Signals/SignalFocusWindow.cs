using Playdarium.SimpleUI.Runtime.Interfaces;

namespace Playdarium.SimpleUI.Runtime.Signals
{
	public readonly struct SignalFocusWindow
	{
		public readonly IWindow Window;

		public SignalFocusWindow(IWindow window)
		{
			Window = window;
		}
	}
}