using Playdarium.SimpleUI.Runtime.Interfaces;

namespace Playdarium.SimpleUI.Runtime.Signals
{
	public readonly struct SignalActiveWindow
	{
		public readonly IWindow Window;

		public SignalActiveWindow(IWindow window)
		{
			Window = window;
		}
	}
}