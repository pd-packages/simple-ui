﻿using Playdarium.SimpleUI.Runtime.Interfaces;
using Playdarium.SimpleUI.Runtime.Models;

namespace Playdarium.SimpleUI.Runtime
{
	public abstract class Window : IWindow
	{
		public abstract string Name { get; }

		public abstract void SetState(UiWindowState state);

		public abstract void Back();

		public abstract IUiElement[] GetUiElements();
	}
}