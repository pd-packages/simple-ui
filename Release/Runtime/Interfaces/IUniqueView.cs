namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IUniqueView<TKey>
	{
		TKey Key { get; }
	}
}