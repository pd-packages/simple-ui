# Changelog

---

## [v1.0.5](https://gitlab.com/pd-packages/simple-ui/-/tags/v1.0.5)

### Changes

- Fix bind multiple WindowState

---

## [v1.0.4](https://gitlab.com/pd-packages/simple-ui/-/tags/v1.0.4)

### Changes

- Fix bind multiple WindowsController

---

## [v1.0.3](https://gitlab.com/pd-packages/simple-ui/-/tags/v1.0.3)

### Changes

- Fix controllers execution order in a window

---

## [v1.0.2](https://gitlab.com/pd-packages/simple-ui/-/tags/v1.0.2)

### Changes

- Fix namespaces

---

## [v1.0.1](https://gitlab.com/pd-packages/simple-ui/-/tags/v1.0.1)

### Added

- AWindowsInstaller

---

## [v1.0.0](https://gitlab.com/pd-packages/simple-ui/-/tags/v1.0.0)

### Changed

- Replace enum to string for Window layers

---

## [v0.0.0](https://gitlab.com/pd-packages/simple-ui/-/tags/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
