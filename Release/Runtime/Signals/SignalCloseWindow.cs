using Playdarium.SimpleUI.Runtime.Interfaces;

namespace Playdarium.SimpleUI.Runtime.Signals
{
	public readonly struct SignalCloseWindow
	{
		public readonly IWindow Window;

		public SignalCloseWindow(IWindow window)
		{
			Window = window;
		}
	}
}