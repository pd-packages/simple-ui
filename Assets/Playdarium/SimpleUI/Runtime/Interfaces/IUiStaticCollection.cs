namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IUiStaticCollection<TView> : IUiListCollectionBase<TView> where TView : IUiView
	{
	}
}