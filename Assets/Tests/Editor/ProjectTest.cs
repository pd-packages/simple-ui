using NUnit.Framework;
using Playdarium.SimpleUI.Runtime;
using Playdarium.SimpleUI.Runtime.Signals;
using Tests.Abstracts;
using Zenject;

namespace SimpleUi
{
	[TestFixture]
	public class ProjectTest : ZenjectUnitTestFixture
	{
		private Actionwords _actionwords;

		protected override void Install(DiContainer container)
		{
			_actionwords = new Actionwords();

			container.BindUiSignals(WindowLayerNames.Local);

			container.BindInterfacesAndSelfTo<FirstController>().AsSingle();
			container.BindInterfacesAndSelfTo<SecondController>().AsSingle();
			container.BindInterfacesAndSelfTo<ThirdController>().AsSingle();
			container.BindInterfacesAndSelfTo<PopUpFirstController>().AsSingle();
			container.BindInterfacesAndSelfTo<PopUpSecondController>().AsSingle();

			container.BindWindow<FirstWindow>(WindowLayerNames.Local);
			container.BindWindow<SecondWindow>(WindowLayerNames.Local);
			container.BindWindow<ThirdWindow>(WindowLayerNames.Local);
			container.BindWindow<FirstPopUpWindow>(WindowLayerNames.Local);
			container.BindWindow<SecondPopUpWindow>(WindowLayerNames.Local);
			container.BindWindow<WindowTwoControllers>(WindowLayerNames.Local);
			container.BindWindow<WindowThreeControllers>(WindowLayerNames.Local);

			container.BindWindowsController<WindowsController>(WindowLayerNames.Local);

			container.Inject(_actionwords);
		}

		[Test]
		public void WindowController()
		{
			_actionwords.OpenFirstIsActive();
			_actionwords.OpenSecondIsActive();
			_actionwords.OpenThirdIsActive();
			_actionwords.OpenPopUpFirstIsActive();
			_actionwords.OpenPopUpSecondIsActive();
			_actionwords.BackPopUpSecondClosed();
			_actionwords.OpenTwoControllerWindowFirstSecondIsActive();
			_actionwords.OpenThreeControllerWindowFirstSecondThirdIsActive();
			_actionwords.BackThreeControllerWindowThirdClosed();
			_actionwords.BackTwoControllerWindowSecondClosedPopUpFirstIsActive();
			_actionwords.BackPopUpFirstPopUpFirstClosedThirdIsActive();
			_actionwords.BackThirdThirdClosedSecondIsActive();
			_actionwords.BackSecondSecondClosedFirstIsActive();
			_actionwords.BackFirstFirstIsActive();
		}
	}
}