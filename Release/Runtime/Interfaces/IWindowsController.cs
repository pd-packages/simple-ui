using System.Collections.Generic;

namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IWindowsController
	{
		IEnumerable<IWindow> Windows { get; }

		void Reset();
	}
}