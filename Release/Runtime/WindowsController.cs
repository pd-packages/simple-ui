using System;
using System.Collections.Generic;
using System.Linq;
using Playdarium.SimpleUI.Runtime.Interfaces;
using Playdarium.SimpleUI.Runtime.Models;
using Playdarium.SimpleUI.Runtime.Signals;
using UniRx;
using UnityEngine;
using Zenject;

namespace Playdarium.SimpleUI.Runtime
{
	public class WindowsController : IWindowsController, IInitializable, IDisposable
	{
		private readonly DiContainer _container;
		private readonly SignalBus _signalBus;
		private readonly Dictionary<Type, IWindow> _windowsByType = new();
		private readonly Dictionary<string, IWindow> _windowsByName = new();
		private readonly WindowState _windowState;
		private readonly string _windowLayer;
		private readonly Stack<IWindow> _windowsStack = new();
		private readonly CompositeDisposable _disposables = new();

		private IWindow _window;

		public IEnumerable<IWindow> Windows => _windowsStack;

		public WindowsController(
			DiContainer container,
			SignalBus signalBus,
			WindowState windowState,
			string windowLayer
		)
		{
			_container = container;
			_signalBus = signalBus;
			_windowState = windowState;
			_windowLayer = windowLayer;
		}

		public void Initialize()
		{
			var windows = _container.ResolveIdAll<IWindow>(_windowLayer);
			foreach (var window in windows)
			{
				var type = window.GetType();
				if (_windowsByType.ContainsKey(type))
				{
					Debug.LogError($"[{nameof(WindowsController)}] Duplicate window type '{type.Name}'");
					continue;
				}

				if (_windowsByName.ContainsKey(window.Name))
				{
					Debug.LogError($"[{nameof(WindowsController)}] Duplicate window name '{window.Name}'");
					continue;
				}

				_windowsByType.Add(type, window);
				_windowsByName.Add(window.Name, window);
			}

			_signalBus.GetStreamId<SignalOpenWindow>(_windowLayer).Subscribe(OnOpen).AddTo(_disposables);
			_signalBus.GetStreamId<SignalBackWindow>(_windowLayer).Subscribe(_ => OnBack()).AddTo(_disposables);
			_signalBus.GetStreamId<SignalOpenRootWindow>(_windowLayer).Subscribe(OnOpenRootWindow).AddTo(_disposables);
		}

		public void Dispose() => _disposables.Dispose();

		private void OnOpen(SignalOpenWindow signal)
		{
			IWindow window;
			if (signal.Type != null)
				_windowsByType.TryGetValue(signal.Type, out window);
			else
				_windowsByName.TryGetValue(signal.Name, out window);

			if (window == null)
			{
				var windowDescription = signal.Type != null
					? $"type '{signal.Type.Name}'"
					: $"name '{signal.Name}'";
				Debug.LogError(
					$"[{nameof(WindowsController)}] No window {windowDescription} in layer '{_windowLayer}'");
				return;
			}

			Open(window);
		}

		private void Open(IWindow window)
		{
			var isNextWindowPopUp = window is IPopUp;
			var currentWindow = _windowsStack.Count > 0 ? _windowsStack.Peek() : null;
			if (currentWindow != null)
			{
				var isCurrentWindowPopUp = currentWindow is IPopUp;
				var isCurrentWindowNoneHidden = currentWindow is INoneHidden;
				if (isCurrentWindowPopUp)
				{
					if (!isNextWindowPopUp)
					{
						var openedWindows = GetPreviouslyOpenedWindows();
						var popupsOpened = GetPopupsOpened(openedWindows);
						var last = openedWindows.Last();
						last.SetState(UiWindowState.NotActiveNotFocus);

						foreach (var openedPopup in popupsOpened)
						{
							openedPopup.SetState(UiWindowState.NotActiveNotFocus);
						}
					}
					else
						currentWindow.SetState(isCurrentWindowNoneHidden
							? UiWindowState.IsActiveNotFocus
							: UiWindowState.NotActiveNotFocus);
				}
				else if (isNextWindowPopUp)
					_window?.SetState(UiWindowState.IsActiveNotFocus);
				else
					_window?.SetState(isCurrentWindowNoneHidden
						? UiWindowState.IsActiveNotFocus
						: UiWindowState.NotActiveNotFocus);
			}

			_windowsStack.Push(window);
			_windowState.CurrentWindowName = window.Name;
			window.SetState(UiWindowState.IsActiveAndFocus);
			_signalBus.FireId(_windowLayer, new SignalShowWindow(window));
			ActiveAndFocus(window, isNextWindowPopUp);
		}

		private void OnBack()
		{
			if (_windowsStack.Count == 0)
				return;

			var currentWindow = _windowsStack.Pop();
			currentWindow.Back();
			_signalBus.FireId(_windowLayer, new SignalCloseWindow(currentWindow));
			OpenPreviousWindows();
		}

		private void OpenPreviousWindows()
		{
			if (_windowsStack.Count == 0)
				return;

			var openedWindows = GetPreviouslyOpenedWindows();
			var openedPopups = GetPopupsOpened(openedWindows);
			var firstWindow = GetFirstWindow();
			var isFirstPopUp = false;

			var isNoPopups = openedPopups.Count == 0;
			var isOtherWindow = firstWindow != _window;
			if (isOtherWindow || isNoPopups)
			{
				firstWindow = openedWindows.Last();
				firstWindow.Back();
				_window = firstWindow;
			}

			if (!isNoPopups)
			{
				var window = openedPopups.Last();
				firstWindow = window;
				isFirstPopUp = true;

				if (isOtherWindow)
				{
					foreach (var openedPopup in openedPopups)
						openedPopup.Back();
				}
				else
				{
					window.Back();
				}
			}

			_windowState.CurrentWindowName = firstWindow.Name;
			ActiveAndFocus(firstWindow, isFirstPopUp);
		}

		private void ActiveAndFocus(IWindow window, bool isPopUp)
		{
			if (!isPopUp)
				_window = window;

			_signalBus.FireId(_windowLayer, new SignalActiveWindow(window));
			_signalBus.FireId(_windowLayer, new SignalFocusWindow(window));
		}

		private List<IWindow> GetPreviouslyOpenedWindows()
		{
			var windows = new List<IWindow>();

			var hasWindow = false;
			foreach (var window in _windowsStack)
			{
				var isPopUp = window is IPopUp;
				if (isPopUp)
				{
					if (hasWindow)
						break;

					windows.Add(window);
					continue;
				}

				if (hasWindow)
					break;
				windows.Add(window);
				hasWindow = true;
			}

			return windows;
		}

		private Stack<IWindow> GetPopupsOpened(List<IWindow> windows)
		{
			var stack = new Stack<IWindow>();

			var hasPopup = false;
			for (var i = 0; i < windows.Count; i++)
			{
				var window = windows[i];
				var isPopUp = window is IPopUp;
				if (!isPopUp)
					break;

				if (hasPopup && !(window is INoneHidden))
					continue;

				stack.Push(window);
				hasPopup = true;
			}

			return stack;
		}

		private IWindow GetFirstWindow()
		{
			foreach (var element in _windowsStack)
			{
				if (element is IPopUp)
					continue;
				return element;
			}

			return null;
		}

		private void OnOpenRootWindow(SignalOpenRootWindow obj)
		{
			while (_windowsStack.Count > 1)
			{
				OnBack();
			}
		}

		public void Reset()
		{
			while (_windowsStack.Count > 0)
			{
				OnBack();
			}

			_window = null;
		}
	}
}