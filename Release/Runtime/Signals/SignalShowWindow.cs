﻿using Playdarium.SimpleUI.Runtime.Interfaces;

namespace Playdarium.SimpleUI.Runtime.Signals
{
	public readonly struct SignalShowWindow
	{
		public readonly IWindow Window;

		public SignalShowWindow(IWindow window)
		{
			Window = window;
		}
	}
}