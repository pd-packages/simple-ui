﻿using Playdarium.SimpleUI.Runtime.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Playdarium.SimpleUI.Runtime.Abstracts
{
	public abstract class UiView : UIBehaviour, IUiView
	{
		public bool IsShow { get; private set; }

		void IUiView.Show()
		{
			gameObject.SetActive(true);
			IsShow = true;
			OnShow();
		}

		protected virtual void OnShow()
		{
		}

		void IUiView.Hide()
		{
			gameObject.SetActive(false);
			IsShow = false;
			OnHide();
		}

		protected virtual void OnHide()
		{
		}

		void IUiView.SetParent(Transform parent)
		{
			transform.SetParent(parent, false);
		}

		void IUiView.SetOrder()
		{
			var parent = transform.parent;
			if (parent == null)
				return;

			transform.SetAsLastSibling();
		}

		IUiElement[] IUiView.GetUiElements()
		{
			return gameObject.GetComponentsInChildren<IUiElement>();
		}

		void IUiView.Destroy()
		{
			Destroy(gameObject);
		}
	}
}