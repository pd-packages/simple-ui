namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IWindowState
	{
		string CurrentWindowName { get; }
	}
}