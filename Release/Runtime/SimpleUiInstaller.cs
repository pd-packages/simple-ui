using UnityEngine;
using Zenject;

namespace Playdarium.SimpleUI.Runtime
{
	public class SimpleUiInstaller : MonoInstaller
	{
		[SerializeField] private string windowLayer;

		public override void InstallBindings()
		{
			Container.BindWindowsController<WindowsController>(windowLayer);
			Container.BindUiSignals(windowLayer);
		}
	}
}
