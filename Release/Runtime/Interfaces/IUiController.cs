﻿using Playdarium.SimpleUI.Runtime.Models;

namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IUiController
	{
		bool IsActive { get; }
		bool InFocus { get; }

		void SetState(UiControllerState state);
		void ProcessStateOrder();
		void ProcessState();
		void Back();
		IUiElement[] GetUiElements();
	}
}