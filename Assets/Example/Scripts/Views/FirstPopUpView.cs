﻿using Playdarium.SimpleUI.Runtime.Abstracts;
using UnityEngine.UI;

namespace Example.Views
{
	public class FirstPopUpView : UiView
	{
		public Button Next;
		public Button Back;
	}
}