﻿using Playdarium.SimpleUI.Runtime;
using Playdarium.SimpleUI.Runtime.Managers;
using Playdarium.SimpleUI.Runtime.Signals;
using Zenject;

namespace Example
{
	public class TestUiInstaller : MonoInstaller
	{
		public override void InstallBindings()
		{
			SignalBusInstaller.Install(Container);

			Container.BindUiSignals(WindowLayerNames.Local);

			Container.BindWindow<FirstWindow>(WindowLayerNames.Local);
			Container.BindWindow<SecondWindow>(WindowLayerNames.Local);
			Container.BindWindow<ThirdWindow>(WindowLayerNames.Local);
			Container.BindWindow<FourthWindow>(WindowLayerNames.Local);
			Container.BindWindow<FifthWindow>(WindowLayerNames.Local);
			Container.BindWindow<FirstPopUpWindow>(WindowLayerNames.Local);
			Container.BindWindow<SecondPopUpWindow>(WindowLayerNames.Local);

			Container.BindInterfacesAndSelfTo<UiFilterManager>().AsSingle();
			Container.BindInterfacesAndSelfTo<TestManager>().AsSingle().NonLazy();

			Container.BindWindowsController<WindowsController>(WindowLayerNames.Local);
		}
	}
}