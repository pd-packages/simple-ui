using Playdarium.SimpleUI.Runtime.Models;

namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IWindow
	{
		string Name { get; }

		void SetState(UiWindowState state);

		void Back();

		IUiElement[] GetUiElements();
	}
}