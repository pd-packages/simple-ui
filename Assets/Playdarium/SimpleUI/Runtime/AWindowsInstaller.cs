using Zenject;

namespace Playdarium.SimpleUI.Runtime
{
	public abstract class AWindowsInstaller : MonoInstaller
	{
		protected abstract string WindowLayer { get; }

		public override void InstallBindings()
		{
			Container.BindUiSignals(WindowLayer);
			Container.BindWindowsController<WindowsController>(WindowLayer);
		}
	}
}