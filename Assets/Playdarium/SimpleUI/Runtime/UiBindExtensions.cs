﻿using System;
using Playdarium.SimpleUI.Runtime.Interfaces;
using Playdarium.SimpleUI.Runtime.Signals;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Playdarium.SimpleUI.Runtime
{
	public static class UiBindExtensions
	{
		public static void BindUiView<T, TU>(this DiContainer container, Object viewPrefab, Transform parent)
			where TU : IUiView
			where T : IUiController
		{
			container.BindInterfacesAndSelfTo<T>().AsSingle();
			container.BindInterfacesAndSelfTo<TU>()
				.FromComponentInNewPrefab(viewPrefab)
				.UnderTransform(parent).AsSingle()
				.OnInstantiated((context, o) => ((MonoBehaviour)o).gameObject.SetActive(false));
		}

		public static void BindUiSignals(this DiContainer container, string windowLayer)
		{
			container.DeclareSignal<SignalOpenWindow>().WithId(windowLayer);
			container.DeclareSignal<SignalOpenRootWindow>().WithId(windowLayer);
			container.DeclareSignal<SignalBackWindow>().WithId(windowLayer);
			container.DeclareSignal<SignalShowWindow>().WithId(windowLayer).OptionalSubscriber();
			container.DeclareSignal<SignalActiveWindow>().WithId(windowLayer).OptionalSubscriber();
			container.DeclareSignal<SignalFocusWindow>().WithId(windowLayer).OptionalSubscriber();
			container.DeclareSignal<SignalCloseWindow>().WithId(windowLayer).OptionalSubscriber();
		}

		public static void BindWindowsController<T>(this DiContainer container, string windowLayer)
			where T : IWindowsController, IInitializable
		{
			container.BindInitializableExecutionOrder<T>(-1000);
			var windowState = new WindowState();
			container.BindInterfacesTo<T>().AsCached().WithArguments(windowState, windowLayer).NonLazy();
			container.Bind<IWindowState>().WithId(windowLayer).To<WindowState>()
				.FromInstance(windowState).AsCached();
		}

		public static void BindWindow<T>(this DiContainer container, Enum windowLayer)
			where T : IWindow
			=> container.BindWindow<T>(windowLayer.ToString());

		public static void BindWindow<T>(this DiContainer container, string windowLayer)
			where T : IWindow
			=> container.Bind<IWindow>().WithId(windowLayer).To<T>().AsCached();
	}
}