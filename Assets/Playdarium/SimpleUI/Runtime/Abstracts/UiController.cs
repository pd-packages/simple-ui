﻿using System.Collections.Generic;
using Playdarium.SimpleUI.Runtime.Interfaces;
using Playdarium.SimpleUI.Runtime.Models;
using Zenject;

namespace Playdarium.SimpleUI.Runtime.Abstracts
{
	public abstract class UiController<T> : IUiController where T : IUiView
	{
		private readonly Stack<UiControllerState> _states = new();
		private readonly UiControllerState _defaultState = new(false, false);

		private UiControllerState _currentState;

		[Inject] protected readonly T View;

		public bool IsActive { get; private set; }
		public bool InFocus { get; private set; }

		public void SetState(UiControllerState state)
		{
			_currentState = state;
			_states.Push(state);
		}

		public void ProcessStateOrder()
		{
			if (!_currentState.IsActive)
				return;

			SetOrder();
		}

		public void ProcessState()
		{
			if (IsActive != _currentState.IsActive)
			{
				IsActive = _currentState.IsActive;
				if (IsActive)
					Show();
				else
					Hide();
			}

			if (InFocus != _currentState.InFocus)
			{
				InFocus = _currentState.InFocus;
				OnHasFocus(_currentState.InFocus);
			}
		}

		public void Back()
		{
			if (_states.Count > 0)
				_states.Pop();

			if (_states.Count == 0)
			{
				_currentState = _defaultState;
				return;
			}

			SetState(_states.Pop());
		}

		IUiElement[] IUiController.GetUiElements() => View.GetUiElements();

		private void Show()
		{
			View.Show();
			OnShow();
		}

		protected virtual void OnShow()
		{
		}

		private void Hide()
		{
			View.Hide();
			OnHide();
		}

		protected virtual void OnHide()
		{
		}

		protected virtual void OnHasFocus(bool inFocus)
		{
		}

		private void SetOrder() => View.SetOrder();
	}
}