﻿using Playdarium.SimpleUI.Runtime.Abstracts;
using UnityEngine.UI;

namespace Example.Views
{
	public class SecondView : UiView
	{
		public Button Next;
		public Button Back;
	}
}