using Playdarium.SimpleUI.Runtime.Interfaces;

namespace Playdarium.SimpleUI.Runtime
{
	public class WindowState : IWindowState
	{
		public string CurrentWindowName { get; set; }
	}
}