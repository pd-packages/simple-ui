using System;
using Zenject;

namespace Playdarium.SimpleUI.Runtime.Signals
{
	public static class SignalExtensions
	{
		public static void OpenRootWindow(this SignalBus signalBus, string windowLayer = WindowLayerNames.Local)
			=> signalBus.FireId<SignalOpenRootWindow>(windowLayer);

		public static void OpenWindow<TWindow>(this SignalBus signalBus, string windowLayer = WindowLayerNames.Local)
			where TWindow : Window
			=> signalBus.FireId(windowLayer, new SignalOpenWindow(typeof(TWindow)));

		public static void OpenWindow(this SignalBus signalBus, Type type,
			string windowLayer = WindowLayerNames.Local)
			=> signalBus.FireId(windowLayer, new SignalOpenWindow(type));

		public static void OpenWindow(this SignalBus signalBus, string name,
			string windowLayer = WindowLayerNames.Local)
			=> signalBus.FireId(windowLayer, new SignalOpenWindow(name));

		public static void BackWindow(this SignalBus signalBus, string windowLayer = WindowLayerNames.Local)
			=> signalBus.FireId<SignalBackWindow>(windowLayer);
	}
}