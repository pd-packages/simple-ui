﻿using UnityEngine;

namespace Playdarium.SimpleUI.Runtime.Interfaces
{
	public interface IUiView
	{
		bool IsShow { get; }

		void Show();
		void Hide();
		IUiElement[] GetUiElements();
		void SetOrder();
		void SetParent(Transform parent);
		void Destroy();
	}
}