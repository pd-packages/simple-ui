﻿namespace Playdarium.SimpleUI.Runtime.Models
{
	public struct UiControllerState
	{
		public bool IsActive;
		public bool InFocus;

		public UiControllerState(bool isActive, bool inFocus)
		{
			IsActive = isActive;
			InFocus = inFocus;
		}
	}
}